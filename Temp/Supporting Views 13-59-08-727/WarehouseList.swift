//
//  WarehouseList.swift
//  AIM_2
//
//  Created by Manuel Garcia on 24/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct WarehouseList: View {
    @ObservedObject var store : Settings
    @State private var searchTerm: String = ""
    private var profileButton: some View {
            Button(action: {
                self.store.logged = false
                print("Ir a content View")
                NavigationLink(destination  : ContentView()) {
                    ContentView()
                }
            }) {
    //            Image(systemName: "person.crop.circle")
                Text("Cerrar sesión")
        }
        }
    var body: some View {
        NavigationView {
            VStack {
                SearchBar(text: $searchTerm)
                List(warehouseData.filter( { self.searchTerm.isEmpty ? true : $0.name.localizedCaseInsensitiveContains(self.searchTerm) })) { warehouse in
                    NavigationLink(destination: WarehouseDetail(warehouse: warehouse)) {
                        WarehouseRow(warehouse: warehouse)
                    }
                }
                .navigationBarTitle(Text("Almacenes"))            .navigationBarItems(trailing: profileButton)

            }

        }
               
    }
    
}

struct WarehouseList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            WarehouseList(store: Settings())
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}
