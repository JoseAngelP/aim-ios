//
//  SupplierProduct.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI
import CoreLocation

struct SupplierProduct: Hashable, Codable, Identifiable{
    
    var id: Int
    var idProduct: Int
    var idSupplier: Int
    var supplierName: String
    var isPrimary: Bool
    var price: Float
    var deliverDays: Int
    var fiability: Float
    //    var createdAt: Date
    //    var updatedAt: Date
}
