//
//  Product.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI
import CoreLocation

struct Product: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var description: String
    var unitFactor: Float
    var movementUnits: MovementUnits
    var line: String
    fileprivate var imageName: String

//    var createdAt: Date
//    var updatedAt: Date
    
}
extension Product {
    var image: Image {
        ImageStore.shared.image(name: imageName)
    }
}


struct MovementUnits: Hashable, Codable{
    var entry: String
    var out: String
}
