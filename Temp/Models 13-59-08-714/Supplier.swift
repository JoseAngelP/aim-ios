//
//  Supplier.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI
import CoreLocation

struct Supplier: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var address: String
    var rfc: String
    var phoneNumber: Int
    var email: String
    var bankAccount: Int
    var preferredPaymentMethod: String
//    var createdAt: Date
//    var updatedAt: Date
}

