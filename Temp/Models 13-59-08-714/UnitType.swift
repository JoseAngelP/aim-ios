//
//  UnitType.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI
import CoreLocation

struct UnitType: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
//    var createdAt: Date
//    var updatedAt: Date
}
