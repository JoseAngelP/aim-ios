//
//  ProductWarehouse.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI
import CoreLocation

struct ProductWarehouse: Hashable, Codable, Identifiable{
    
    var id: Int
    var idProduct: Int
    var idWarehouse: Int
    var amount: Float
    //    var createdAt: Date
    //    var updatedAt: Date
}
