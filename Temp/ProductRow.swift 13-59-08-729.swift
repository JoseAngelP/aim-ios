//
//  ProductRow.swift
//  AIM_2
//
//  Created by Manuel Garcia on 03/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct ProductRow: View {
    var product : Product
    var warehouse : ProductWarehouse
    
    var body: some View {
        HStack {
            Rectangle()
                .frame(width: 50, height: 50)
                .padding(.leading,10)
                
            VStack {
                HStack {
                    Text(product.name)
                    Spacer()
                    Text(String(warehouse.amount) + " " + product.movementUnits.entry.uppercased().prefix(2))
                    .font(.headline)
                    .bold()
                }
                
                HStack {
                    Text(product.line)
                        .font(.caption)
                    Spacer()
                }
            }
            .fixedSize(horizontal: false, vertical: true)
            .padding(.leading,10)
            
        }
    }
}

struct ProductRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ProductRow(product: productData[0], warehouse: productWarehouse[0])
            ProductRow(product: productData[0], warehouse: productWarehouse[0])
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}

