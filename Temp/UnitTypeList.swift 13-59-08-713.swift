//
//  UnitTypeList.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct UnitTypeList: View {
    
    @State private var searchTerm: String = ""
    
    var body: some View {
    
            VStack {
                SearchBar(text: $searchTerm)
                
                List(unitTypeData.filter( { self.searchTerm.isEmpty ? true : $0.name.localizedCaseInsensitiveContains(self.searchTerm) })) { unitType in
                    UnitTypeRow(unitType: unitType)
                    
                }.padding()
                .navigationBarTitle(Text("Tipos de unidades"))
            }
        }
    
}

struct UnitTypeList_Previews: PreviewProvider {
    static var previews: some View {
        
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            UnitTypeList()                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}
