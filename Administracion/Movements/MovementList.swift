//
//  MovementList.swift
//  Administracion
//
//  Created by Manuel Garcia on 01/11/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI
import Firebase

struct MovementList: View {
    @State private var searchTerm: String = ""
    @State private var inventoryMovements : [InventoryMovement] = []
    @State private var loaded = false
    @ObservedObject var Managers = ManagersObject()
    
    var body: some View {
        VStack {
            SearchBar(text: $searchTerm)
            
            List(inventoryMovements.filter( { self.searchTerm.isEmpty ? true : $0.product!.localizedCaseInsensitiveContains(self.searchTerm) })) { movement in
                MovementRow(movement: movement)
                
            }.onAppear { self.getInventoryMovements() }
                .padding()
                .navigationBarTitle(Text("Movimientos"))
        }
    }
    
    func getInventoryMovements(){
        if self.loaded == false {

            Managers.mDataManager.getInventoryMovements().getDocuments{ (documentSnapshot, err) in

                if (err != nil) {
                    print("Error")
                    return
                }
                var movements: [InventoryMovement] = []
                for document in (documentSnapshot?.documents)! {
                    print(document.data())
                    let movement = InventoryMovement(inventoryMovementData: document.data())
                    
                    print(movement.toStringId())
                    
                    movements.append(movement)
                }
                print(movements)
                self.inventoryMovements = movements
                
            }
            self.loaded = true
        }

        
    }
}

struct MovementList_Previews: PreviewProvider {
    static var previews: some View {
        MovementList()
    }
}
