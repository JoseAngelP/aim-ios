//
//  FirebaseImage.swift
//  Administracion
//
//  Created by Manuel Garcia on 31/10/19.
//  Copyright © 2019 Manuel Garcia. All rights reserved.
//

import SwiftUI

let placeholder = UIImage(named: "placeholder.jpeg")!

struct FirebaseImage : View {

    init(id: String) {
        self.imageLoader = Loader(id)
    }

    @ObservedObject private var imageLoader : Loader

    var image: UIImage? {
        imageLoader.data.flatMap(UIImage.init)
    }

    var body: some View {
        Image(uiImage: image ?? placeholder).resizable().scaledToFit()
    }
}
