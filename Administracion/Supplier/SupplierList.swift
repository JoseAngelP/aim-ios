//
//  SupplierList.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct SupplierList: View {
    
    @State private var searchTerm: String = ""
    @State private var suppliers : [Supplier] = []
    @State private var loaded = false
    @ObservedObject var Managers = ManagersObject()
    
    var body: some View {
            
//            ForEach(supplierData){ supplier in
//                Text(supplier.name)
//            }
            VStack{
                SearchBar(text: $searchTerm)
                List(suppliers.filter( { self.searchTerm.isEmpty ? true : $0.name!.localizedCaseInsensitiveContains(self.searchTerm) })) {
                    supplier in
                    NavigationLink(destination: SupplierDetail(supplier: supplier)) {
                        SupplierRow(supplier: supplier)
                    }
                }.onAppear { self.getSuppliers() }
            }

            .navigationBarTitle(Text("Proveedores"))
               
    }
    
    func getSuppliers(){

        if self.loaded == false {

            Managers.mDataManager.getSuppliers().getDocuments{ (documentSnapshot, err) in

                if (err != nil) {
                    print("Error")
                    return
                }
                var supps: [Supplier] = []
                for document in (documentSnapshot?.documents)! {
                    print(document.data())
                    let supplier = Supplier(supplierData: document.data())
                    supps.append(supplier)
                }
                print(supps)
                self.suppliers = supps
                
            }
            self.loaded = true
        }

        
    }
}

struct SupplierList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            SupplierList()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}
