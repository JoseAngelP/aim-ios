//
//  ProductList.swift
//  AIM_2
//
//  Created by Manuel Garcia on 03/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct ProductList: View {
    @State private var searchTerm: String = ""
    @State private var products : [Product] = []
    @State private var loaded = false
    var warehouseProducts: [[String: Any]]
    @ObservedObject var Managers = ManagersObject()


    var body: some View {

            
//            ForEach(supplierData){ supplier in
//                Text(supplier.name)
//            }
            VStack{
                SearchBar(text: $searchTerm)
                List(products.filter( { self.searchTerm.isEmpty ? true :$0.name!.localizedCaseInsensitiveContains(self.searchTerm)})) {
                    product in
                    NavigationLink(destination: ProductDetail(product: product)) {
                        ProductRow(product: product)
                    }
                }.onAppear { self.getProducts() }
            }

            .navigationBarTitle(Text("Productos"))
        
               
    }
    
    func getProducts(){
        
        if !loaded {
            for product in warehouseProducts{
                let amount = product["amount"] as! Float
                let productId = product["productId"] as! String
                
                Managers.mDataManager.getProduct(productId: productId).getDocument{ (document, error) in
                    if let document = document, document.exists {
                        var newProduct =  Product(productData: document.data()!)
                        newProduct.amount = amount
                        self.products.append(newProduct)
                    } else {
                        print("Document does not exist")
                    }
                }
                
            }
            self.loaded = true
        }

    }
}

struct ProductList_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            ProductList(warehouseProducts: Warehouse.getDefault().products!)
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}

