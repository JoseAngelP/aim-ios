//
//  ProductDetail.swift
//  AIM_2
//
//  Created by Manuel Garcia on 26/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct ProductDetail: View {
    var product : Product
    
    var body: some View {
        ScrollView{
            VStack {
                Rectangle()
                .fill(Color.gray)
                .frame(height: 300)
                
                FirebaseImage(id: product.imageId!)       .clipShape(Circle())
                .overlay(Circle().stroke(Color.white, lineWidth: 4))
                .shadow(radius: 10)
                                .offset(x: 0, y: -130)
                                .padding(.bottom, -130)
                
                VStack(alignment: .leading) {
                    HStack {
                        Text(product.name!)
                            .font(.title)
                        Spacer()
                        Text(String(product.amount!))
                            .font(.largeTitle)
                            .bold()
                            .fixedSize(horizontal: false, vertical: true)

                    }
                    
                    HStack(alignment: .top) {
                        Text(product.line!)
                            .fixedSize(horizontal: false, vertical: true)
                            .font(.subheadline)
                        Spacer()
                        Text(product.entryUnit!)
                            .fixedSize(horizontal: false, vertical: true)
                            .font(.subheadline)
                    }
                    HStack{
                        Text(product.description!)
                            .fixedSize(horizontal: false, vertical: true)
                        .padding(.top, 50)

                    }
                    HStack{
                        Text("Se empaca en \(product.outUnit!)")
                        .lineLimit(nil)
                            .fixedSize(horizontal: false, vertical: true)
                            .font(.subheadline)

                        .padding(.top, 50)

                    }

                  
                    
                }.padding()
            }.fixedSize(horizontal: false, vertical: true)
            
        }.navigationBarTitle(Text(verbatim: product.name!), displayMode: .inline)
    }
}

struct ProductDetail_Previews: PreviewProvider {
    static var previews: some View {
        ProductDetail(product: Product.getDefault())
    }
}

