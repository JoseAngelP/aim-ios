//
//  UnitTypeList.swift
//  AIM_2
//
//  Created by Manuel Garcia on 25/09/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import SwiftUI

struct UnitTypeList: View {
    
    @State private var searchTerm: String = ""
    @State private var unitTypes : [UnitType] = []
    @State private var loaded = false
    @ObservedObject var Managers = ManagersObject()


    
    var body: some View {

            VStack {
                SearchBar(text: $searchTerm)
                
                List(unitTypes.filter( { self.searchTerm.isEmpty ? true : $0.name!.localizedCaseInsensitiveContains(self.searchTerm) })) { unitType in
                    UnitTypeRow(unitType: unitType)
                    
                }.onAppear { self.getUnitTypes() }
                    .padding()
                .navigationBarTitle(Text("Tipos de unidades"))
            }
        
    }
    
    func getUnitTypes(){
        if self.loaded == false {

            Managers.mDataManager.getUnitTypes().getDocuments{ (documentSnapshot, err) in

                if (err != nil) {
                    print("Error")
                    return
                }
                var units: [UnitType] = []
                for document in (documentSnapshot?.documents)! {
                    print(document.data())
                    let unit = UnitType(unitTypeData: document.data())
                    units.append(unit)
                }
                print(units)
                self.unitTypes = units
                
            }
            self.loaded = true
        }

        
    }
    
}

struct UnitTypeList_Previews: PreviewProvider {
    static var previews: some View {
        
        ForEach(["iPhone SE", "iPhone XS Max"], id: \.self) { deviceName in
            UnitTypeList()                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
        }
    }
}
