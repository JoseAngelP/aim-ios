//
//  UserUtils.swift
//  AIM
//
//  Created by Manuel Garcia on 9/11/19.
//  Copyright © 2019 Percept. All rights reserved.
//
import Foundation
import Firebase
import FirebaseAuth

class UserUtils {
    
    class func getCurrent() -> User?{
        
        if let currentUser = Auth.auth().currentUser {
            return currentUser
        } else {
            return nil
        }
    }
    
    class func isAuthenticated() -> Bool {
        return Auth.auth().currentUser != nil
    }
    
    class func getUid() -> String{
        return getCurrent()?.uid ?? "UID"
    }
    
    class func getEmail() -> String {
        
        return getCurrent()?.email ?? "correo@ejemplo.com"
    }
    
    class func getName() -> String {
        
        return getCurrent()?.displayName ?? "Nombre Apellido"
        
        
    }
    
    class func getAvatarURL() -> URL {
        
        return getCurrent()?.photoURL ?? URL(fileURLWithPath: "nourl.com")
        
    }
    
    class func signIn(
        email: String,
        password: String,
        handler: @escaping AuthDataResultCallback
        ) {
        Auth.auth().signIn(withEmail: email, password: password, completion: handler)
    }
    
    class func logOut(){
        try! Auth.auth().signOut()
    }
}

