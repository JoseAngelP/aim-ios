//
//  ColorUtils.swift
//  AIM
//
//  Created by Manuel Garcia on 9/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import UIKit

class ColorUtils {
    
    public static let colorPrimary = UIColor(rgb: 0x5092FF)
    public static let colorPrimaryDark = UIColor(rgb: 0x2E4970)
    public static let colorAccent = UIColor(rgb: 0x00D3C8)
    
    public static let white = UIColor(rgb: 0xFFFFFF)
    public static let black = UIColor(rgb: 0x000000)
    
    public static let grey5 = UIColor(rgb: 0x2E486F)
    public static let grey4 = UIColor(rgb: 0x6D7896)
    public static let grey3 = UIColor(rgb: 0x8F9EB2)
    public static let grey2 = UIColor(rgb: 0xC1C9D4)
    public static let grey1 = UIColor(rgb: 0xF2F3F6)
    
    public static let red = UIColor(rgb: 0xFF6968)
    public static let orange = UIColor(rgb: 0xFF8F4F)
    public static let yellow = UIColor(rgb: 0xFFC74F)
    public static let green = UIColor(rgb: 0x88D076)
    public static let fosfo = UIColor(rgb: 0x12D89D)
    public static let cyan = UIColor(rgb: 0x00D3C8)
    public static let blue = UIColor(rgb: 0x3EBDFF)
    public static let strong_blue = UIColor(rgb: 0x5092FF)
    public static let purple = UIColor(rgb: 0x7A7CFF)
    public static let lavender = UIColor(rgb: 0xB590FF)
    public static let pink = UIColor(rgb: 0xFF7DAB)
    
    public static let _grey5 = UIColor(rgb: 0xB22E486F)
    
    class func tint(view v: UIImageView, withColor: UIColor) {
        v.image = v.image!.withRenderingMode(.alwaysTemplate)
        v.tintColor = withColor
    }
    
    class func setColorMultiply(image: UIImage, color: UIColor) -> UIImage {
        return (UIImage.multiply(image: image, color: color)!)
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

extension UIImage {
    
    class func multiply(image: UIImage, color: UIColor) -> UIImage? {
        let rect = CGRect(origin: .zero, size: image.size)
        
        //image colored
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let coloredImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //image multiply
        UIGraphicsBeginImageContextWithOptions(image.size, true, 0)
        let context = UIGraphicsGetCurrentContext()
        
        // fill the background with white so that translucent colors get lighter
        context!.setFillColor(UIColor(rgb: 0xF2F3F6).cgColor)
        context!.fill(rect)
        
        image.draw(in: rect, blendMode: .normal, alpha: 1)
        coloredImage?.draw(in: rect, blendMode: .screen, alpha: 1)
        
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return result
    }
}

