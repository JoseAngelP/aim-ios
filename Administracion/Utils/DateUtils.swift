//
//  DateUtils.swift
//  AIM
//
//  Created by Manuel Garcia on 9/10/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import FirebaseFirestore

class DateUtils {
    
    class func getMonthDate(timeStamp: Timestamp) -> String{
        
        let date = Date(timeIntervalSince1970: TimeInterval(timeStamp.seconds))
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MM"
        
        let month = Int(dateFormatter.string(from: date))
        
        dateFormatter.dateFormat = "d"
        
        return String(AppConstants.MONTHS[month! - 1] + " " + dateFormatter.string(from: date))
    }
    
    class func getDaysToDate(timeStamp: Timestamp) -> Int{
        let timeBetweenDates = timeStamp.seconds - Int64(Date().timeIntervalSince1970)
        return Int(timeBetweenDates / 86400)
    }
    
    class func getDayMonthYear(t: Timestamp) -> String{
        
        let date = Date(timeIntervalSince1970: TimeInterval(t.seconds))
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "d MMM, yyyy"
        
        return dateFormatter.string(from: date)
    }
    
    class func getDayMonthYear(l: Int64) -> String{
        
        let date = Date(timeIntervalSince1970: TimeInterval(l/1000))
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "d MMM, yyyy"
        
        return dateFormatter.string(from: date)
    }
    
    class func getHour(timestamp: Timestamp) -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp.seconds))
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "hh:mm"
        
        let calendar = Calendar.current
        let comp = calendar.dateComponents([.hour, .minute], from: date)
        return AppConstants.HOURS[comp.hour!]
    }
    
    class func getTime(l: Int64) -> String{
        let date = Date(timeIntervalSince1970: TimeInterval(l))
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "hh:mm"
        
        return dateFormatter.string(from: date)
    }
    
}

