//
//  State.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import Firebase
import FirebaseFirestore

class Location{
    var id : String?
    var name : String?
    
    init(stateData : [String:Any]) {
        id = stateData["id"] as? String
        name = stateData["name"] as? String
    }
}
