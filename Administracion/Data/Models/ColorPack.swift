//
//  ColorPack.swift
//  AIM
//
//  Created by Manuel Garcia on 9/14/19.
//  Copyright © 2019 Percept. All rights reserved.
//

import Foundation
import UIKit

class ColorPack {
    
    var color: String?
    
    public func getColor() -> UIColor{
        return AppConstants.objectColors[getColorIndex()]
    }
    
    public func getColorIndex() -> Int{
        return AppConstants.stringColors.firstIndex(of: color!)!
    }
}
